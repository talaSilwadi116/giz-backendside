<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				background-color: #B0BEC5;
				color:white;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
    <body>
		<div class="container">
			<div class="content">
				<div class="title">GIZ | Dashboard</div>
                <div><a class="btn btn-link" href="{{ URL('/login') }}">Login</a></div>
			</div>
		</div>
	</body>
</html>
<?php /**PATH /Applications/MAMP2/htdocs/giz-backendside/resources/views/welcome.blade.php ENDPATH**/ ?>